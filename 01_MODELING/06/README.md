## 01 - bowling
![bowlingImg](01-bowling/01_01.png "bowling")
![bowlingImg](01-bowling/01_02.png "bowling")

## 02 - chess set 
![chessImg](02-chess/00.png "chess")
![pawnsImg](02-chess/01_pawns.png "pawns")
![knightsImg](02-chess/02_knights.png "knights")
![bishopsImg](02-chess/03_bishops.png "bishops")
![rooksImg](02-chess/04_rooks.png "rooks")
![kingsImg](02-chess/05_kings.png "kings")
![queensImg](02-chess/06_queens.png "queens")
