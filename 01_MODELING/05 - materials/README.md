# MATERIALS

## SHADDERS
## 01 
![xxx](img/01_01.png "xxx")
## 02
![xxx](img/02_01.png "xxx")
## 03
![xxx](img/03_01.png "xxx")
## 04
![xxx](img/04_01.png "xxx")
![xxx](img/04_02.png "xxx")
### Glossy
![xxx](img/04_03.png "xxx")
### Principled
![xxx](img/04_04.png "xxx")
![xxx](img/04_05.png "xxx")
### Refraction
![xxx](img/04_06.png "xxx")
### 3x Glass 
![xxx](img/04_07.png "xxx")
### Translucent (blue)
![xxx](img/04_08.png "xxx")
### Big cube (Fog) - Volume Absorption + Volume Scatter
### Small cube - Emission
![xxx](img/04_09.png "xxx")
![xxx](img/04_10.png "xxx")


## TEXTURES
## 05
![xxx](img/05_01.png "xxx")
![xxx](img/05_02.png "xxx")
### Glass (color) [magic]
![xxx](img/05_03.png "xxx")
## 06
### Scratched 'plastic'
![xxx](img/06_01.png "xxx")
### Scratches (big & small combined)
![xxx](img/06_02.png "xxx")
### Lava ball
![xxx](img/06_03.png "xxx")
### 'Smooth' orange
![xxx](img/06_04.png "xxx")


## 07
![xxx](img/07_01.png "xxx")
## 08
![xxx](img/08_01.png "xxx")
![xxx](img/08_02.png "xxx")
## 09
![xxx](img/09_01.png "xxx")
## 10
![xxx](img/10_01.png "xxx")
## 11
![xxx](img/11_01.png "xxx")
## 12
![xxx](img/12_01.png "xxx")
![xxx](img/12_02.png "xxx")
## 13
![xxx](img/13_01.png "xxx")
![xxx](img/13_02.png "xxx")
## 14
![xxx](img/14_01.png "xxx")
## 15
![xxx](img/15_01.png "xxx")
## 16
![xxx](img/16_01.png "xxx")
![xxx](img/16_02.png "xxx")


