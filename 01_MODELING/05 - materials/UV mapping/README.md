# 11 - UV mapping

## Voronoi mapping
### voronoi - 1d
![xxx](01_01.png "xxx")
### voronoi - 2d
![xxx](01_02.png "xxx")
### voronoi - 3d
![xxx](01_03.png "xxx")

## Image - GENERATED mapping
### generated - FLAT
![xxx](01_04.png "xxx")
### generated - BOX
![xxx](01_05.png "xxx")
### generated - BOX with BLEND (blur seams)
![xxx](01_06.png "xxx")
### generated - SPHERE (wrap around)
![xxx](01_07.png "xxx")


## Cube - UV mapping
![xxx](01_08.png "xxx")

## UV mapping - seams
![xxx](01_09.png "xxx")

