# MODELING

## 01 - Blocks & snowman  
![blocks](img/01_01.png "Blocks")
![snowman](img/01_02.png "Snowman")

## 02- Trucks
![Trucks](img/02_01.png "Trucks")  
![Trucks](img/02_02.png "Trucks")  
![Trucks](img/02_03.png "Trucks")  

## 03 -Shapes & desk  
### Blocks
![Shapes](img/03_01.png "Shapes")  
![Shapes](img/03_02.png "Shapes")  
### Chair
![Chair](img/03_03.png "Chair")  
### Desk
![Desk](img/03_04.png "Desk")  
![Desk](img/03_05.png "Desk")  

## 04 - Barrel
![Barrel](img/04_01.png "Barrel")
![Barrel](img/04_02.png "Barrel")
![Barrel](img/04_03.png "Barrel")
![Barrel](img/04_04.png "Barrel")


